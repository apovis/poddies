#!/usr/bin/env bash

# Version: 2024.06.03
set -o nounset
set -o errexit
#set -o noclobber
#set -o noglob
set -x

eval TIMESTAMP="$(date +"%Y%m%d%H%M")"
CONTAINER_NAME="rockymini"
VERSION="Minimal"
ROOTFS_TAR_XZ="Rocky-9-Container-${VERSION}.latest.x86_64.tar.xz"
TAR_URL="https://dl.rockylinux.org/pub/rocky/9/images/x86_64/${ROOTFS_TAR_XZ}"

# build a minimal image
newcontainer=$(buildah from scratch)
scratchmnt=$(buildah mount ${newcontainer})

# download tar
curl -LOC - ${TAR_URL}

# extract alpine rootfs
echo "extracting mini-rootfs"
bsdtar -xf ${ROOTFS_TAR_XZ} -C ${scratchmnt}

# run updates and clean cache
buildah run ${newcontainer} -- microdnf --refresh upgrade -y
buildah run ${newcontainer} -- microdnf clean all
buildah run ${newcontainer} -- rm -rf /var/cache/microdnf/*

buildah config --cmd /bin/bash ${newcontainer}
#buildah config ----entrypoint '["command", "arg1", ...]'
#buildah config ----entrypoint '["htop"]'

# set some config info
buildah config --label name=${CONTAINER_NAME} ${newcontainer}

# commit the image
buildah umount ${newcontainer}
buildah commit ${newcontainer} ${CONTAINER_NAME}:${TIMESTAMP}
