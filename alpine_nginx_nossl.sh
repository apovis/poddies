#!/usr/bin/env bash

# Version: 2025.01.20
set -o nounset
set -o errexit
#set -o noclobber
#set -o noglob
#set -x

eval TIMESTAMP="$(date +"%Y%m%d%H%M")"
CONTAINER_NAME="alpinexnossl"
ALPINE_VERSION_MINOR="2"
ALPINE_VERSION_MAJOR="3.21"
ROOTFS_TAR_GZ="alpine-minirootfs-${ALPINE_VERSION_MAJOR}.${ALPINE_VERSION_MINOR}-x86_64.tar.gz"
ROOTFS_TAR_GZ_URL="https://dl-cdn.alpinelinux.org/alpine/v${ALPINE_VERSION_MAJOR}/releases/x86_64/${ROOTFS_TAR_GZ}"

NGINX_DOCROOT="/usr/share/nginx/html"

# download rootfs
curl -LOC - ${ROOTFS_TAR_GZ_URL}

# init new "working-container-digit"
newcontainer=$(buildah from scratch)

# mount new container
scratchmnt=$(buildah mount ${newcontainer})

# extract alpine rootfs
echo "extracting mini-rootfs"
bsdtar -xf ${ROOTFS_TAR_GZ} -C ${scratchmnt}

# copy apk repo files and update system
buildah copy ${newcontainer} files/apk_repos /etc/apk/repositories 

# update new rootfs
buildah run ${newcontainer} -- apk upgrade -U --no-cache

# install webserver
buildah run ${newcontainer} -- apk add -U --no-cache nginx openssl nghttp2 nghttp3

# create doc-root dir and copy local config files to container
buildah run ${newcontainer} -- mkdir -p ${NGINX_DOCROOT}
buildah run ${newcontainer} -- mkdir -p /etc/nginx/conf.d
buildah run ${newcontainer} -- mkdir -p /etc/nginx/http.d
buildah copy ${newcontainer} files/nginx.conf /etc/nginx/nginx.conf
buildah copy ${newcontainer} files/index_nginx.html ${NGINX_DOCROOT}/index.html
buildah copy ${newcontainer} files/nginx_default_80_only.conf /etc/nginx/http.d/default.conf
buildah run ${newcontainer} -- passwd -l root

# change access rights
buildah run ${newcontainer} -- chown -R root:root /etc/nginx/

# what to start, when container starts
buildah config --entrypoint '["nginx", "-g", "daemon off;"]' ${newcontainer}
#buildah config --entrypoint "/bin/ash" ${newcontainer}

# set label for container
buildah config --label name=${CONTAINER_NAME} ${newcontainer}

# commit/save the image and set a tag
buildah umount ${newcontainer}
buildah commit ${newcontainer} ${CONTAINER_NAME}:${TIMESTAMP}
