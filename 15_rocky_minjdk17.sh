#!/usr/bin/env bash

# Version: 2024.06.03
set -o nounset
set -o errexit
#set -o noclobber
#set -o noglob
set -x

eval TIMESTAMP="$(date +"%Y%m%d%H%M")"
CONTAINER_NAME="rockyminjdk17"
VERSION="Minimal"
ROOTFS_TAR_XZ="Rocky-9-Container-${VERSION}.latest.x86_64.tar.xz"
ROOTFS_TAR_URL="https://dl.rockylinux.org/pub/rocky/9/images/x86_64/${ROOTFS_TAR_XZ}"

# download rootfs
curl -LOC - ${ROOTFS_TAR_URL}

# build a minimal image
newcontainer=$(buildah from scratch)
scratchmnt=$(buildah mount ${newcontainer})

# extract alpine rootfs
echo "extracting mini-rootfs"
bsdtar -xf ${ROOTFS_TAR_GZ} -C ${scratchmnt}

# copy files
echo "temurin repo to /etc/yum.repos.d/adoptium.repo"
buildah copy ${newcontainer} files/adoptium.repo /etc/yum.repos.d/adoptium.repo

# run updates and clean cache
buildah run ${newcontainer} -- microdnf --refresh upgrade -y
buildah run ${newcontainer} -- microdnf install -y temurin-17-jdk
buildah run ${newcontainer} -- microdnf clean all
buildah run ${newcontainer} -- rm -rf /var/cache/microdnf/*

buildah config --cmd /bin/bash ${newcontainer}
#buildah config ----entrypoint '["command", "arg1", ...]'
#buildah config ----entrypoint '["htop"]'

# set some config info
buildah config --label name=${CONTAINER_NAME} ${newcontainer}

# commit the image
buildah umount ${newcontainer}
buildah commit ${newcontainer} ${CONTAINER_NAME}:${TIMESTAMP}
