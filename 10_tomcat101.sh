#!/usr/bin/env bash

# Version: 2025.01.07
set -o nounset
set -o errexit
#set -o noclobber
#set -o noglob
set -x

eval TIMESTAMP="$(date +"%Y%m%d%H%M")"
# alpine nginx container
CONTAINER_NAME="tomcat101"
TOMCAT_VERSION="10.1.33"
TOMCAT_MAJOR="10"
ALPINE_VERSION_MINOR="1"
ALPINE_VERSION_MAJOR="3.21"
ROOTFS_TAR_GZ="alpine-minirootfs-${ALPINE_VERSION_MAJOR}.${ALPINE_VERSION_MINOR}-x86_64.tar.gz"
TOMCAT_TAR_GZ="apache-tomcat-${TOMCAT_VERSION}.tar.gz"
ROOTFS_URL="https://dl-cdn.alpinelinux.org/alpine/v${ALPINE_VERSION_MAJOR}/releases/x86_64/${ROOTFS_TAR_GZ}"
TOMCAT101_URL="https://dlcdn.apache.org/tomcat/tomcat-${TOMCAT_MAJOR}/v${TOMCAT_VERSION}/bin/apache-tomcat-${TOMCAT_VERSION}.tar.gz"

# get alpine rootfs and tomcat 10.1
curl -LOC - ${ROOTFS_URL}
curl -LOC - ${TOMCAT101_URL}

# build a minimal image
newcontainer=$(buildah from scratch)
scratchmnt=$(buildah mount ${newcontainer})

# extract alpine rootfs
echo "extracting mini-rootfs"
bsdtar -xf ${ROOTFS_TAR_GZ} -C ${scratchmnt}

# copy apk repo files and update system
echo "copying apk_repos to /etc/apk/repositories"
buildah copy ${newcontainer} files/apk_repos /etc/apk/repositories 
buildah run ${newcontainer} -- apk upgrade -U --no-cache

# install java
buildah run ${newcontainer} -- apk add -U --no-cache openjdk17-jdk coreutils curl tomcat-native

buildah run ${newcontainer} -- mkdir -p /home/tomcat
buildah run ${newcontainer} -- addgroup -g 1000 tomcat
buildah run ${newcontainer} -- adduser -h /home/tomcat -s /bin/sh -G tomcat -D -u 1000 tomcat
bsdtar -xf ${TOMCAT_TAR_GZ} -C ${scratchmnt}/home/tomcat
buildah run ${newcontainer} -- mkdir -p /home/tomcat/apache-tomcat-10.1.16/conf/Catalina/localhost/
buildah run ${newcontainer} -- ln -sf /home/tomcat/apache-tomcat-10.1.16 /home/tomcat/tomcat10
buildah copy ${newcontainer} files/tomcat-users.xml /home/tomcat/tomcat10/conf/
buildah copy ${newcontainer} files/manager.xml /home/tomcat/tomcat10/conf/Catalina/localhost/
buildah copy ${newcontainer} files/host-manager.xml /home/tomcat/tomcat10/conf/Catalina/localhost/
buildah run ${newcontainer} -- chown -R tomcat: /home/tomcat/
buildah run ${newcontainer} -- passwd -l root

buildah config --cmd 'run' ${newcontainer}
buildah config --env CATALINA_HOME="/home/tomcat/tomcat10"  ${newcontainer}
buildah config --env JRE_HOME="/usr/lib/jvm/java-17-openjdk"  ${newcontainer}
buildah config --env "PATH=/bin:/sbin:/usr/bin:/usr/sbin:/usr/local/sbin:/usr/local/bin:/usr/bin/vendor_perl/:/usr/lib/jvm/java-17-openjdk/bin:/home/tomcat/tomcat10/bin"  ${newcontainer}
buildah config --env "LD_LIBRARY_PATH=/home/tomcat/tomcat10/lib"  ${newcontainer}
buildah config --user tomcat  ${newcontainer}
buildah config --workingdir /home/tomcat/  ${newcontainer}
buildah config --entrypoint '["/home/tomcat/tomcat10/bin/catalina.sh"]' ${newcontainer}
#buildah config --entrypoint "/bin/ash" ${newcontainer}

# set some config info
buildah config --label name=${CONTAINER_NAME} ${newcontainer}

# commit the image
buildah umount ${newcontainer}
buildah commit ${newcontainer} ${CONTAINER_NAME}:${TIMESTAMP}
