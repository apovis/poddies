#!/usr/bin/env bash

# Version: 2024.12.04
set -o nounset
set -o errexit
#set -o noclobber
#set -o noglob
#set -x

eval TIMESTAMP="$(date +"%Y%m%d%H%M")"
CONTAINER_NAME="chimera"
VERSION="20241204"
ROOTFS_TAR_GZ="chimera-linux-x86_64-ROOTFS-${VERSION}-bootstrap.tar.gz"
ROOTFS_TAR_GZ_URL="https://repo.chimera-linux.org/live/latest/${ROOTFS_TAR_GZ}"

# download rootfs
curl -LOC - ${ROOTFS_TAR_GZ_URL}

# build a minimal image
newcontainer="$(buildah from scratch)"
scratchmnt="$(buildah mount ${newcontainer})"

# extract void musl rootfs and update packages
bsdtar -xvf ${ROOTFS_TAR_GZ} -C ${scratchmnt}
buildah run ${newcontainer} -- /usr/sbin/apk update -U
buildah run ${newcontainer} -- /usr/sbin/apk upgrade --no-cache -al --no-interactive
buildah run ${newcontainer} -- /usr/sbin/apk add --no-cache -luq --no-progress --no-interactive chimera-repo-contrib
buildah run ${newcontainer} -- /usr/sbin/apk update -U
buildah run ${newcontainer} -- /usr/sbin/apk add --no-cache -luq --no-progress --no-interactive bash htop

#buildah config --entrypoint '["command", "arg1", ...]'
buildah config --cmd /usr/sbin/bash ${newcontainer}

# set some config info
buildah config --label name=${CONTAINER_NAME} ${newcontainer}

# commit the image
buildah umount ${newcontainer}
buildah commit ${newcontainer} ${CONTAINER_NAME}:${TIMESTAMP}
