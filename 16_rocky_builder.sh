#!/usr/bin/env bash

# Version: 2024.06.03
set -o nounset
set -o errexit
#set -o noclobber
#set -o noglob
#set -x

eval TIMESTAMP="$(date +"%Y%m%d%H%M")"
CONTAINER_NAME="rockybuilder"
VERSION="Minimal"
ROOTFS_TAR_XZ="Rocky-9-Container-${VERSION}.latest.x86_64.tar.xz"
ROOTFS_TAR_URL="https://dl.rockylinux.org/pub/rocky/9/images/x86_64/${ROOTFS_TAR_XZ}"

# build a minimal image
newcontainer=$(buildah from scratch)
scratchmnt=$(buildah mount ${newcontainer})

BUILD_USER="buildbot"

# download tar
curl -LOC - ${ROOTFS_TAR_URL}

# extract alpine rootfs
echo "extracting mini-rootfs"
bsdtar -xf ${ROOTFS_TAR_XZ} -C ${scratchmnt}

# run updates, install software and clean cache
buildah run ${newcontainer} -- microdnf --refresh upgrade -y
buildah run ${newcontainer} -- microdnf install -y tmux go openssl-devel make gcc automake systemd-devel openldap-devel bison vim git bsdtar
buildah run ${newcontainer} -- microdnf clean all
buildah run ${newcontainer} -- rm -rf /var/cache/microdnf/*

# create user
buildah run ${newcontainer} -- useradd -mU -s /bin/bash ${BUILD_USER}

# copy config files
buildah copy ${newcontainer} files/bash_profile /home/${BUILD_USER}/.bash_profile
buildah copy ${newcontainer} files/bashrc_user /home/${BUILD_USER}/.bashrc
buildah copy ${newcontainer} files/vim.tar.xz /home/${BUILD_USER}/
buildah copy ${newcontainer} files/tmux.conf /etc/tmux.conf
buildah run ${newcontainer} -- bsdtar xJf /home/${BUILD_USER}/vim.tar.xz -C /home/${BUILD_USER}/
buildah run ${newcontainer} -- rm -f /home/${BUILD_USER}/vim.tar.xz 

# set rights for user
buildah run ${newcontainer} -- chown -R ${BUILD_USER}:${BUILD_USER} /home/${BUILD_USER}

# lock root
buildah run ${newcontainer} -- usermod --lock root

# set entrypoint for container
buildah config --cmd /bin/bash ${newcontainer}
buildah config --user ${BUILD_USER} ${newcontainer}
buildah config --workingdir /home/${BUILD_USER} ${newcontainer}
#buildah config ----entrypoint '["command", "arg1", ...]'
#buildah config ----entrypoint '["htop"]'

# set some config info
buildah config --label name=${CONTAINER_NAME} ${newcontainer}

# commit the image
buildah umount ${newcontainer}
buildah commit ${newcontainer} ${CONTAINER_NAME}:${TIMESTAMP}
