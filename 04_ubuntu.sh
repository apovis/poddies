#!/usr/bin/env bash

# Version: 2024.06.03
set -o nounset
set -o errexit
#set -o noclobber
#set -o noglob
set -x

eval TIMESTAMP="$(date +"%Y%m%d%H%M")"
# 2204
CODENAME="noble"
# build a minimal image
newcontainer=$(buildah from scratch)
scratchmnt=$(buildah mount ${newcontainer})
CONTAINER_NAME="ubuntu_${CODENAME}"


# install the packages
debootstrap --variant=minbase --arch=amd64 ${CODENAME} ${scratchmnt} http://de.archive.ubuntu.com/ubuntu

buildah config --cmd /bin/bash ${newcontainer}

# set some config info
buildah config --label name=${CONTAINER_NAME} ${newcontainer}

# commit the image
buildah unmount ${newcontainer}
buildah commit ${newcontainer} ${CONTAINER_NAME}:${TIMESTAMP}
