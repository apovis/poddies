#!/usr/bin/env bash

# Version: 2024.06.03
set -o nounset
set -o errexit
#set -o noclobber
#set -o noglob
set -x

eval TIMESTAMP="$(date +"%Y%m%d%H%M")"
CODENAME="micro"
# build a minimal image
#rockymicrocontainer="$(buildah from registry.access.redhat.com/ubi9/ubi-micro)"
rockymicrocontainer="$(buildah from scratch)"
rockymicromnt="$(buildah mount ${rockymicrocontainer})"
CONTAINER_NAME="rocky_${CODENAME}"

# install the packages
dnf --repo=rocky-base --repo=rocky-apps --repo=rocky-extras --repo=adoptium --releasever=9 --exclude=iwl*,openssh*,grub* --best --setopt=install_weak_deps=False --nodocs -y --installroot=${rockymicromnt} install temurin-17-jdk
dnf clean all --installroot=${rockymicromnt}
rm -rf ${rockymicromnt}/var/vache/dnf/*

buildah config --cmd /bin/bash ${rockymicrocontainer}

# set some config info
buildah config --label name=${CONTAINER_NAME} ${rockymicrocontainer}

# commit the image
buildah umount ${rockymicrocontainer}
buildah commit ${rockymicrocontainer} ${CONTAINER_NAME}:${TIMESTAMP}
