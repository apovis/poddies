#!/usr/bin/env bash

# Version: 2024.06.03
set -o nounset
set -o errexit
#set -o noclobber
#set -o noglob
#set -x

eval TIMESTAMP="$(date +"%Y%m%d%H%M")"
CONTAINER_NAME="archy"
# build a minimal image
newcontainer=$(buildah from scratch)
scratchmnt=$(buildah mount ${newcontainer})

# install the packages
pacstrap ${scratchmnt} base neovim glibc curl rsync tmux 
buildah copy ${newcontainer} files/bashrc /root/.bashrc
buildah copy ${newcontainer} files/bash_profile /root/.bash_profile
buildah copy ${newcontainer} files/tmux.conf /etc/tmux.conf
buildah copy ${newcontainer} files/locale.gen /etc/

#buildah run ${newcontainer} -- /usr/bin/locale-gen

buildah config --cmd /bin/bash ${newcontainer}
buildah config --workingdir /root ${newcontainer}

# set some config info
buildah config --label name=${CONTAINER_NAME} ${newcontainer}

# commit the image
buildah umount ${newcontainer}
buildah commit ${newcontainer} ${CONTAINER_NAME}:${TIMESTAMP}

exit 0
