#!/usr/bin/env bash

# Version: 2024.06.03
set -o nounset
set -o errexit
#set -o noclobber
#set -o noglob
set -x

eval TIMESTAMP="$(date +"%Y%m%d%H%M")"
CODENAME="bookworm"
# build a minimal image
newcontainer="$(buildah from scratch)"
scratchmnt="$(buildah mount ${newcontainer})"
CONTAINER_NAME="debian_${CODENAME}"

# install the packages
#debootstrap --arch=amd64 --variant=minbase --include=systemd,dbus ${CODENAME} ${scratchmnt} http://ftp.de.debian.org/debian/
debootstrap --arch=amd64 --variant=minbase ${CODENAME} ${scratchmnt} http://ftp.de.debian.org/debian/

buildah config --cmd /bin/bash ${newcontainer}

# set some config info
buildah config --label name=${CONTAINER_NAME} ${newcontainer}

# commit the image
buildah umount ${newcontainer}
buildah commit ${newcontainer} ${CONTAINER_NAME}:${TIMESTAMP}
