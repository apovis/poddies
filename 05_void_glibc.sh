#!/usr/bin/env bash

# Version: 2024.06.03
set -o nounset
set -o errexit
#set -o noclobber
#set -o noglob
#set -x

eval TIMESTAMP="$(date +"%Y%m%d%H%M")"
CONTAINER_NAME="voidglibc"
VERSION="20240314"
ROOTFS_TAR_XZ="void-x86_64-ROOTFS-${VERSION}.tar.xz"
ROOTFS_TAR_XZ_URL="https://repo-default.voidlinux.org/live/current/${ROOTFS_TAR_XZ}"

# download rootfs
curl -LOC - ${ROOTFS_TAR_XZ_URL}

# build a minimal image
newcontainer=$(buildah from scratch)
scratchmnt=$(buildah mount ${newcontainer})

# extract void musl rootfs and update packages
bsdtar -xvf ${ROOTFS_TAR_XZ} -C ${scratchmnt}
buildah run ${newcontainer} -- /usr/bin/xbps-install -Sy xbps
buildah run ${newcontainer} -- /usr/bin/xbps-install -Syu
buildah run ${newcontainer} -- /usr/sbin/rm -f /var/cache/xbps/*

#buildah config --entrypoint '["command", "arg1", ...]'
buildah config --cmd /bin/bash ${newcontainer}

# set some config info
buildah config --label name=${CONTAINER_NAME} ${newcontainer}

# commit the image
buildah umount ${newcontainer}
buildah commit ${newcontainer} ${CONTAINER_NAME}:${TIMESTAMP}

exit 0
