#!/usr/bin/env bash

# Version: 2025.01.20
set -o nounset
set -o errexit
#set -o noclobber
#set -o noglob
#set -x

eval TIMESTAMP="$(date +"%Y%m%d%H%M")"
# alpine nginx container
CONTAINER_NAME="alpinexssl"
ALPINE_VERSION_MINOR="2"
ALPINE_VERSION_MAJOR="3.21"
ROOTFS_TAR_GZ="alpine-minirootfs-${ALPINE_VERSION_MAJOR}.${ALPINE_VERSION_MINOR}-x86_64.tar.gz"
ROOTFS_TAR_GZ_URL="https://dl-cdn.alpinelinux.org/alpine/v${ALPINE_VERSION_MAJOR}/releases/x86_64/${ROOTFS_TAR_GZ}"

# download rootfs
curl -LOC - ${ROOTFS_TAR_GZ_URL}

# build a minimal image
newcontainer=$(buildah from scratch)
scratchmnt=$(buildah mount ${newcontainer})

# extract alpine rootfs
echo "extracting mini-rootfs"
bsdtar -xf ${ROOTFS_TAR_GZ} -C ${scratchmnt}

# copy apk repo files and update system
echo "copying apk_repos to /etc/apk/repositories"
buildah copy ${newcontainer} files/apk_repos /etc/apk/repositories 
buildah run ${newcontainer} -- apk upgrade -U --no-cache

# install webstuff
buildah run ${newcontainer} -- apk add -U --no-cache nginx openssl nghttp2 nghttp3
buildah run ${newcontainer} -- mkdir -p /srv/nginx/wild/
buildah run ${newcontainer} -- mv /etc/nginx/nginx.conf{,.bak}

# copy files into container 
buildah copy ${newcontainer} files/nginx.conf /etc/nginx/nginx.conf
buildah copy ${newcontainer} files/wild-dream.conf /etc/nginx/http.d/wild-dream.conf
buildah copy ${newcontainer} files/dh2048.pem /etc/nginx/dh2048.pem
buildah copy ${newcontainer} files/certs /etc/nginx/certs
buildah copy ${newcontainer} files/index.html /srv/nginx/wild/index.html

# change access rights
buildah run ${newcontainer} -- chown -R root:root /srv/nginx/wild/
buildah run ${newcontainer} -- chown -R root:root /etc/nginx/

buildah config --entrypoint '["nginx", "-g", "daemon off;"]' ${newcontainer}
#buildah config --entrypoint "/bin/sh" ${newcontainer}
#buildah config --entrypoint '["tmux"]' ${newcontainer}

# set some config info
buildah config --label name=${CONTAINER_NAME} ${newcontainer}

# commit the image
buildah umount ${newcontainer}
buildah commit ${newcontainer} ${CONTAINER_NAME}:${TIMESTAMP}
